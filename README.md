# Charge Rework

## The Purpose
Introduce by Studio Wildcard in Aberration, the Charge System, has been widely known and used for various creatures, structures, and items. However, when making the system they didn't take Modders into account, so, that's what our focus is. To create a deployable system that Modders can slot in their Mods to add quick, lightweight,easily understandable, and well documented Charge functionality to their Mods.

## Contributing
if you wish to conribute to the development of this Rework, please do so by downloading your preferred Git platform, IE: GitKraken. From there clone the Development Branch and work from there.

## Project Status
In Development: **Version: 0.0.0**

## Version Control
When pushing changes to the Development Branch, please use this format for your versioning: 0.0.0 
The first "0" is for major updates, the second "0" is for minor updates, the third "0" is for bug fixes and slight changes on the fly.
